﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Oracle.ManagedDataAccess.Client;


namespace OracleConnect
{
    class Program
    {
        static void Main(string[] args)
        {
            string queryString = "select * from job_history";
            var connectionString = "Data Source=localhost/xe;User Id=hr;Password=hr;";

            Console.WriteLine("by msdn");
            using (OracleConnection connection = new OracleConnection(connectionString))
            {
                OracleCommand command = new OracleCommand(queryString, connection);
                connection.Open();
                OracleDataReader reader;
                reader = command.ExecuteReader();

                var columnCount = reader.FieldCount;
                Console.WriteLine("columnCount : {0}", columnCount);

                for (int i = 0; i < reader.FieldCount; i++)
                {
                    Console.WriteLine("fieldName : {0}, fieldType : {1}",reader.GetName(i),reader.GetFieldType(i));
                }


                // Always call Read before accessing data.
                while (reader.Read())
                {
                    Console.WriteLine(reader.GetInt32(0));
                }

                // Always call Close when done reading.
                reader.Close();
            }

            Console.WriteLine("mlasknij na koniec");
            Console.ReadKey();
        }
    }
}

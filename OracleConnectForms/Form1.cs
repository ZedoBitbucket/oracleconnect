﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OracleConnectForms
{
    public partial class Form1 : Form
    {

        public Form1()
        {
            InitializeComponent();
            pictureBox1.Image = new Bitmap(pictureBox1.Width, pictureBox1.Height);

            string queryString = "select * from job_history";
            var connectionString = "Data Source=localhost/xe;User Id=hr;Password=hr;";

            Console.WriteLine("by msdn");
            using (OracleConnection connection = new OracleConnection(connectionString))
            {
                OracleCommand command = new OracleCommand(queryString, connection);
                connection.Open();

                DataTable dt = new DataTable();
                OracleDataAdapter oda = new OracleDataAdapter();
                oda.SelectCommand = command;
                oda.Fill(dt);

                dataGridView1.DataSource = dt;
            }



        }

        private void Form1_MouseMove(object sender, MouseEventArgs e)
        {
            
        }

        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            labelx.Text = e.X.ToString();
            labely.Text = e.Y.ToString();
            ((Bitmap)pictureBox1.Image).SetPixel(e.X, e.Y, Color.Red);
            pictureBox1.Refresh();
        }

        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            labelx.Text = e.X.ToString();
            labely.Text = e.Y.ToString();
            ((Bitmap)pictureBox1.Image).SetPixel(e.X, e.Y, Color.Red);
            pictureBox1.Refresh();
        }

        private void Form1_MouseDown(object sender, MouseEventArgs e)
        {

        }

        private void Form1_MouseUp(object sender, MouseEventArgs e)
        {

        }
    }
}
